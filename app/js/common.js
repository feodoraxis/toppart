$(function() {
    $("[type=tel]").mask("+7 (999) 999-99-99");

    var delay = 300;

    $(".main-header .personal").click(function(){
        $(this).toggleClass("active").find("ul").fadeToggle(delay);
    }); 

    $(document).mouseup(function (e) {
		var personal = $(".main-header .personal"); 
		if (!personal.is(e.target) && personal.has(e.target).length === 0) {
            personal.find("ul").fadeOut(delay);
            $(".main-header .personal").removeClass("active");
		}
    });
    
    $(".hamburger").click(function(){
        $(this).toggleClass("is-active");
        $("#mobile-menu").fadeToggle(delay);
    });


});
