var syntax        = 'sass',
    main_sass_src = 'app/' + syntax + '/**/*.'+syntax; // Syntax: sass or scss;

var gulp          = require('gulp'),
    gutil         = require('gulp-util' ),
    sass          = require('gulp-sass'),
    browsersync   = require('browser-sync'),
    concat        = require('gulp-concat'),
    uglify        = require('gulp-uglify'),
    cleancss      = require('gulp-clean-css'),
    rename        = require('gulp-rename'),
    autoprefixer  = require('gulp-autoprefixer'),
    notify        = require("gulp-notify"),
    rsync         = require('gulp-rsync'),
    sourcemaps 	  = require('gulp-sourcemaps'),
    fileinclude   = require('gulp-file-include'),
    changed       = require('gulp-changed');


// Libs concat & minify
gulp.task('js_libs', function() {
    return gulp.src([
            'app/libs/jquery/dist/jquery.min.js',
            'app/libs/bootstrap/popper.min.js',
            'app/libs/bootstrap/bootstrap.js',
            'app/libs/maskedInput/jquery.maskedinput.js',
        ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify()) // Mifify js (opt.)
        .pipe(gulp.dest('app/js'))
        .pipe(browsersync.reload({ stream: true }))
});

// Browser sync
gulp.task('browser-sync', function() {
    browsersync({
        server: {
            baseDir: 'app'
        },
        notify: false,
        open: true,
        // startPath: "/page-portfolio.html"
        // tunnel: true,
        // tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
    })
});

// all css/sass/scss libs
gulp.task('css_libs', function() {
    return gulp.src('app/sass/all_libs.sass')
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'expand' }).on('error', notify.onError(false)))
        .pipe(rename('all_libs.min.css'))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/css'))
        .pipe(browsersync.reload( {stream: true} ))
});

// main styles (no media)
gulp.task('main_styles', function(obj) {
    return gulp.src('app/sass/main.sass')
        .pipe(changed(main_sass_src))
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'nested' }).on('error', function(err){
            console.log(err);
        }))
        .pipe(rename('main.css'))
        .pipe(autoprefixer(['last 15 versions']))
        // .pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/css'))
        .pipe(browsersync.reload( {stream: true} ))
});

//media-styles
gulp.task('media_styles', function() {
    return gulp.src('app/sass/media.sass')
        .pipe(changed(main_sass_src))
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'nested' }).on('error', function(err){
            console.log(err);
        }))
        .pipe(rename('media.css'))
        .pipe(autoprefixer(['last 15 versions']))
        // .pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/css'))
        .pipe(browsersync.reload( {stream: true} ))
});


// Include HTML
gulp.task('fileinclude', function(){
    gulp.src(['app/_html/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }).on("error", notify.onError()))
        .pipe(gulp.dest('app/'))
        .pipe(browsersync.reload({ stream: true }))
});

gulp.task('styles', ['css_libs', 'main_styles', 'media_styles']);

// Watch tasks
gulp.task('watch', ['fileinclude', 'styles', 'js_libs', 'browser-sync'], function() {
    gulp.watch(main_sass_src, ['styles']);
    gulp.watch('app/_html/**/*.html', ['fileinclude']);
    gulp.watch('app/js/common.js', browsersync.reload);
    gulp.watch('app/*.html', browsersync.reload);
});

gulp.task('default', ['watch']);
